<aside class="sidebar shadowright roundlrg borderlight">
	<h1>Sidebar</h1>
	<p>Need some reusable content in a sidebar? No problem, just put it in <code>inc/sidebar.php</code> and call it,  wherever you want it to appear:</p>
	<p><code>include &quot;inc/sidebar.php&quot;;</code></p>
	<p>The image below is being dynamically resized by CSS.</p>
	<img class="roundsm" src="http://placekitten.com/300/300" width="300" height="300" alt="Just a placeholder image">
</aside>